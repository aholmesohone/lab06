import java.util.Random;
public class Jackpot {
	
    public static void main(String[] args){
		
		System.out.println("welcome to this extremely engaging game of jackpot.");
		
		Board board = new Board();
		boolean gameOver = false;
		int numOfTilesClosed = 0;
		
		while(gameOver != true){
			System.out.println(board);
			
			if(board.playATurn() == true){
				gameOver = true;
			}
			else{
				numOfTilesClosed++;
			}
			
		}
		
		if(numOfTilesClosed >= 7){
			System.out.println("You reached the jackpot and won!");
		}
		else{
			System.out.println("You only got "+numOfTilesClosed+" tiles, You lost the game");
		}

    }
}