import java.util.Random;
public class Board {
	private Die first;
	private Die second;
	private Boolean[] tiles = new Boolean[12];

    public Board() {
        this.first = new Die();
        this.second = new Die();
		for(int i=0; i<12; i++){
			this.tiles[i] = false;
		}
    }
	
	public boolean playATurn(){
		first.roll();
		second.roll();
		
		int firstResult = first.getFaceValue();
		int secondResult = second.getFaceValue();
		
		System.out.println(firstResult);
		System.out.println(secondResult);
		
		int sumOfDice = firstResult + secondResult;
		
		if(this.tiles[sumOfDice-1] == false){
			this.tiles[sumOfDice-1] = true;
			System.out.println("Closing tile equal to the sum: " +sumOfDice);
			return false;
		}
		else if(this.tiles[firstResult-1] == false){
			this.tiles[firstResult-1] = true;
			System.out.println("Closing tile matching die 1: " +firstResult);
			return false;
		}
		else if(this.tiles[secondResult-1] == false){
			this.tiles[secondResult-1] = true;
			System.out.println("Closing tile matching die 2: " +secondResult);
			return false;
		}
		else{
			System.out.println("All tiles for these values are already shut");
			return true;
		}
		
	}
	
	
	
	public String toString(){
		String output ="";
		for(int i=0; i<12; i++){
			if(this.tiles[i] == true){
				output += "X";
			}
			else{
				output += (i+1);
			} 
			
			if(i != 11){
				output += " ";
			}
		}
		return output;
	}
	
	
}